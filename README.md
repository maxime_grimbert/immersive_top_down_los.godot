# Godot immersive top-down LOS

An immersive set up for 2D, top-down, 3rd person, exploration games. The playable character can't see behind wall, where the lights are out or behind them and neither can the player.

This implies :

- necessity to rotate the field of view / camera to see and progress throught what is to be explored
- possibility to set up entire maps with lights and shadows that the player discover only when the character does

## For gamers

For now, you'd only find a sample scene and not an actual game. Future releases may provide a full game, who knows ?

## For game devs

If you want to check it out, you can simply download the repo and play the main scene which has been thought of to be a minimal sample.

Contributions are welcomed.

To achieve this result, what I did was :

- [ ] set up a canvas modulate, full opaque black
- [ ] set up lights in environment in add mode, with white and/or alpha texture, colored in white by default but can be colored in the editor and activate transparent shadows with some gradient
- [ ] set up a light on the character whose texture is plain transparent and larger than the camera size in mix mode with full opaque black shadows without gradient but with shadow filter and some smooth

I'm afraid this set up prevents from using lights in mask mode (though you can use visibility and shadows layers as much as you want to hide / reveal things to your character).
