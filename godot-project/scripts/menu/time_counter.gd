extends Label


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var accum = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	accum += delta # c'est donc exprimé en seconde
	text = str(int(accum)) # str makes a string froom a number ; int makes an integer from a float (so that the text changes only once every second, no decimal)
