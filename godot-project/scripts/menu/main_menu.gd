extends Panel


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var l = 1

# Called when the node enters the scene tree for the first time.
func _ready():
	get_node("Ready").connect("pressed", self, "_on_Ready_pressed")
	#get_node("Level")
	get_node("Level0").connect("pressed", self, "_on_Level0_pressed")
	#get_node("Level0Text")
	get_node("Level100").connect("pressed", self, "_on_Level100_pressed")
	#get_node("Level100Text")
	
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _on_Ready_pressed():
	get_node("LevelQuestion").visible = true
	get_node("Level0").visible = true
	get_node("Level100").visible = true
		
func _on_Level0_pressed():
	get_node("Level100Text").visible = false
	get_node("Level0Text").visible = true
	if l is Label :
		pass
	else :
		l = Label.new()
		add_child(l)
		l.text = "We're all set"
		l.rect_position.x = 40
		l.rect_position.y = 160
	
func _on_Level100_pressed():
	get_node("Level0Text").visible = false
	get_node("Level100Text").visible = true
	if l is Label :
		pass
	else :
		l = Label.new()
		add_child(l)
		l.text = "We're all set"
		l.rect_position.x = 40
		l.rect_position.y = 160

#func _on_Button_button_down():
#	get_node("Label").text = "Shit"

#func _on_Button_button_up():
#	get_node("Label").text = "Double shit"
