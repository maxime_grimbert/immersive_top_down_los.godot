extends KinematicBody2D

func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	pass

# Rotation

var initial_mouse_position = Vector2(960,540)
var mouse_position = initial_mouse_position

func _input(event):
	if event is InputEventMouseMotion:
#		print("[DEBUG] Mouse Motion at: ", event.position)
		var new_mouse_position = event.position
		if new_mouse_position.x > mouse_position.x:
			rotation_degrees += 2.5
		elif new_mouse_position.x < mouse_position.x:
			rotation_degrees -= 2.5
		mouse_position = new_mouse_position
		check_and_warp()

func check_and_warp():
	if mouse_position.x < 20:
		get_viewport().warp_mouse(initial_mouse_position)
		mouse_position = initial_mouse_position
	if mouse_position.x > 1900:
		get_viewport().warp_mouse(initial_mouse_position)
		mouse_position = initial_mouse_position
		
	if rotation_degrees == 360:
		rotation_degrees = 0
	if rotation_degrees == -360:
		rotation_degrees = 0

# Déplacement

export (int) var speed = 400

var velocity = Vector2(0,0)

func _physics_process(delta):
	velocity = Vector2(0,0)
	if ( Input.is_action_pressed("IG_focus") != true):
		if Input.is_action_pressed("left_click"):
			velocity.y -= speed
		elif Input.is_action_pressed("right_click"):
			velocity.y += speed*0.5
	velocity = velocity.rotated(rotation)
	velocity = move_and_slide(velocity)
