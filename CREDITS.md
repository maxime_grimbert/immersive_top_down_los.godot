# CREDITS

## _Forgotten Adventures_

This Map was created using the Assets from Forgotten Adventures.

More precisly, most of the top-down assets used in the game are from them.

More info about their copyright : Creative Commons License Attribution-NonCommercial  CC BY-NC-SA, https://www.forgotten-adventures.net/info/

## _New York Public Library_

Gitlab project thumbnail image was downloaded from the NYPL.

More info about their copyright : Public domain collection, « No permission required, no hoops to jump through: just go forth and reuse! », https://www.nypl.org/blog/2016/01/05/share-public-domain-collections